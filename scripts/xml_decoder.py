#! /usr/bin/env python3
import xml.etree.ElementTree as ET
import pandas as pd
import sys
import json


class TrackingData:
    def __init__(self, date, name, df):
        self.date = date
        self.name = name
        self.df = df

    def to_record(self):
        name = self.date + "_" + self.name + ".json"
        name = name.replace("/", "_")
        df = self.df
        y = [(i, list(df[i])) for i in df.columns]

        return {"date": self.date, "name": self.name, "data": dict(y)}


hidariue = complex(139.759262, 35.717919)
hidarisita = complex(139.759414, 35.717325)
migiue = complex(139.760410, 35.718101)
migisita = complex(139.760551, 35.717505)
y = hidarisita - hidariue


def calc(p1, p2, z):
    p2 = p2 - p1
    z = p1 - z
    return abs(p2.real * z.imag - p2.imag * z.real) / abs(p2)


yoko = 68
tate = 105


def conversion(tmp):
    x = complex(tmp["longtitude"], tmp["latitude"])
    d1 = calc(hidariue, hidarisita, x)
    d2 = calc(hidariue, migiue, x)
    d3 = calc(migiue, migisita, x)
    d4 = calc(hidarisita, migisita, x)

    tmp["X"] = d1 * tate / (d1 + d3)
    tmp["Y"] = d2 * yoko / (d2 + d4)
    return tmp


def get_data_from_path(path):
    # parser = ET.XMLParser(recover=True)
    tree = ET.parse(path)
    root = tree.getroot()

    # 気合
    session_summary = root[1][1].text
    name = session_summary.split(",")[0][2:-1]
    session_data = root[1][10]

    data = session_data.text.split("\n")[1:-1]

    # list of (idx, name, parser)
    tag = [(0, 'date', str), (1, 'time', str), (2, 'velocity', float),
           (3, 'heart', int), (6, 'longtitude', float), (7, 'latitude', float),
           (8, 'x', float), (9, 'y', float)]
    ans = []
    for i in data:
        s = i.split(",")
        s = list(map(lambda x: x[1:-1], s))
        s = s[0].split(" ") + s[1:]
        tmp = dict([(key, f(s[i])) for (i, key, f) in tag])
        ans.append(conversion(tmp))
    sorted(ans, key=lambda x: x["time"])

    date = ans[0]["date"]

    df = pd.DataFrame.from_dict(ans)
    df = df.drop('date', 1)
    return TrackingData(date, name, df)


# for j in root[1][13]:
#     print(j)

# print(s)
# for i, c in enumerate(session_data):
#     print(i, c)

# 0 <Element 'distance-mode' at 0x1028503b0>
# 1 <Element 'session-summary' at 0x102850410>
# 2 <Element 'mhr-based-exertion-times' at 0x102850470>
# 3 <Element 'session-sprints' at 0x102850530>
# 4 <Element 'collision-threshold' at 0x102850890>
# 5 <Element 'collision-count' at 0x1028508f0>
# 6 <Element 'collisions-summary' at 0x102850950>
# 7 <Element 'collisions_details' at 0x102850a10>
# 8 <Element 'accel-decel-entries' at 0x102850a70>
# 9 <Element 'reference-data' at 0x102850ad0>
#10 <Element 'session-data' at 0x102850bf0>
#11 <Element 'session-accelerometer-data' at 0x1030e50b0>
#12 <Element 'session-bodyload-data' at 0x1030e52f0>
#13 <Element 'session-impact-data' at 0x1030e5350>
#14 <Element 'session-impact-time-data' at 0x1030e55f0>
#15 <Element 'session-impact-force-distribution-data' at 0x103449050>

if __name__ == "__main__":
    args = sys.argv
    if len(args) <= 1:
        print("usage: python3 xml_decoder.py file.xml")
        exit(0)
    name = args[1]
    import os
    js = get_data_from_path(name).to_json()

    # y = [
    #     get_data_from_path(name + "/" + file).to_record()
    #     for file in os.listdir(name)
    # ]
    #
    with open("test.json", "w") as write_file:
        json.dump(y, write_file)
